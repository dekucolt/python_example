import unittest

from algo import fib


class TestStringMethods(unittest.TestCase):

    def test_ok_1(self):
        self.assertEqual(fib(5), 5, "test failed")

    def test_ok_2(self):
        self.assertEqual(fib(3), 2, "test failed")

    def test_ok_3(self):
        self.assertEqual(fib(1), 1, "test failed")

    def test_ok_4(self):
        self.assertEqual(fib(2), 1, "test failed")

    def test_input_string(self):
        self.assertEqual(fib("somerandomstring"), "only integer must be passed as an argument")


if __name__ == '__main__':
    unittest.main()
