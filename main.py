# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import click
from algo import fib


# def fib_test():
#    assert fib(1) == 1, "Error, 1st element of fibonacci is 1"
#    assert fib("somerandomstring") == "only integer must be passed as an argument",

@click.group()
def cli():
    """nth fibonacci number calculator"""
    pass


@cli.command()
@click.argument("num")
def calculate(num):
    """Calculates you n-th fibonacci number"""
    res = fib(int(num))
    click.echo("result is : {}".format(res))


if __name__ == '__main__':
    cli()
