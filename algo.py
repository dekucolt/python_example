
def fib(n):
    if type(n) != int:
        return "only integer must be passed as an argument"
    if n <= 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)